// Import Required Modules

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const router = express.Router()
const conf = require('./conf.js')

//Use Body Parser
app.use(bodyParser.json())
app.conf = conf

//Port No
var port = 8081


//Server Node 
var server = require('http').Server(app)
console.log("Node listening on " + port)
server.listen(port)


//API route
var APIRoutes = require('./routes/api-routes')
new APIRoutes(app, router)

