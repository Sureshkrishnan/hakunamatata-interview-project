const mongoose = require('mongoose')

const distributorSchema = mongoose.Schema({
    "distributor_nam": {
        "type": String,
        "required": true
    },
    "access_include_city_code": {
        "type": Array,
        "required": true,
        "default": []
    },
    "access_exclude_city_code": {
        "type": Array,
        "required": true,
        "default": []
    }
})

const citySchema = mongoose.Schema({
    "city_code": {
        "type": String,
        "required": true
    },
    "province_code": {
        "type": String,
        "required": true
    },
    "country_code": {
        "type": String,
        "required": true
    },
    "city_nam": {
        "type": String,
        "required": true
    },
    "province_nam": {
        "type": String,
        "required": true
    },
    "country_nam": {
        "type": String,
        "required": true
    }
})

const DistributorModel = mongoose.model('distributors', distributorSchema)
const CityModel = mongoose.model('cities', citySchema)

module.exports = { DistributorModel, CityModel }
