var { CityModel } = require("../models/models")

var City = function (app, router) {
    this.app = app;
    this.router = router;
}

module.exports = City;

City.prototype.cityActions = function (req, res) {
    const self = this;
    var action = req['params']['action'];
    if (action === 'add') {
        self.addCity(req, res)
    } else if (action === 'list') {
        self.listCity(req, res)
    } else if (action === 'update') {
        self.updateCity(req, res)
    } else if (action === 'delete') {
        self.deleteCity(req, res)
    }
}

City.prototype.addCity = function (req, res) {

    var reqObj = req.body;

    var cityName = reqObj.city_nam
    var provinceName = reqObj.province_nam
    var countryName = reqObj.country_nam

    reqObj['city_nam'] = cityName.toUpperCase()
    reqObj['province_nam'] = provinceName.toUpperCase()
    reqObj['country_nam'] = countryName.toUpperCase()


    CityModel.insertMany(reqObj, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "City added successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in inserting City" })
        }
    })
}

City.prototype.listCity = function (req, res) {

    var reqObj = req.body;

    CityModel.find({}, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "City details fetched successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in fetching City details" })
        }
    })
}

City.prototype.updateCity = function (req, res) {

    var reqObj = req.body;

    var CityId = req.body._id;

    var cityName = reqObj.city_nam
    var provinceName = reqObj.province_nam
    var countryName = reqObj.country_nam

    reqObj['city_nam'] = cityName.toUpperCase()
    reqObj['province_nam'] = provinceName.toUpperCase()
    reqObj['country_nam'] = countryName.toUpperCase()

    CityModel.findByIdAndUpdate(CityId, reqObj, { useFindAndModify: false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "City details Updated successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in updating City details" })
        }
    })
}


City.prototype.deleteCity = function (req, res) {

    var reqObj = req.body;

    var CityId = req.body._id;

    CityModel.findByIdAndDelete(CityId, { useFindAndModify: false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "City details deleted successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in deleting City details" })
        }
    })
}