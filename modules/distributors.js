var { DistributorModel } = require("../models/models")
var { CityModel } = require("../models/models")

var Distributor = function (app, router) {
    this.app = app;
    this.router = router;
}

module.exports = Distributor;

Distributor.prototype.distributorActions = function (req, res) {
    const self = this;
    var action = req['params']['action'];
    if (action === 'add') {
        self.addDistributor(req, res)
    } else if (action === 'list') {
        self.listDistributor(req, res)
    } else if (action === 'update') {
        self.updateDistributor(req, res)
    } else if (action === 'delete') {
        self.deleteDistributor(req, res)
    } else if (action === 'checkAccess') {
        self.checkAccess(req, res)
    }
}

Distributor.prototype.addDistributor = function (req, res) {

    var reqObj = req.body;

    DistributorModel.insertMany(reqObj, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "Distributor added successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in inserting Distributor" })
        }
    })
}

Distributor.prototype.listDistributor = function (req, res) {

    var reqObj = req.body;

    DistributorModel.find(reqObj, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "Distributor details fetched successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in fetching distributor details" })
        }
    })
}

Distributor.prototype.updateDistributor = function (req, res) {

    var reqObj = req.body;

    var DistributorId = req.body._id;

    DistributorModel.findByIdAndUpdate(DistributorId, reqObj, { useFindAndModify: false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "Distributor details Updated successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in updating distributor details" })
        }
    })
}


Distributor.prototype.deleteDistributor = function (req, res) {

    var DistributorId = req.body._id;

    DistributorModel.findByIdAndDelete(DistributorId, { useFindAndModify: false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result, message: "Distributor details deleted successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in deleting distributor details" })
        }
    })
}

Distributor.prototype.checkAccess = function (req, res) {

    var reqObj = req.body.city_details;

    var cityName = reqObj.split('-')[0]

    var stateName = reqObj.split('-')[1]

    var countryName = reqObj.split('-')[2]

    var distributorName = req.body.distributor_nam

    var query = {
        $and: [
            { "city_nam": cityName },
            { "province_nam": stateName },
            { "country_nam": countryName }
        ]
    }

    CityModel.find(query, function (err, result) {
        if (!err) {
            if (result.length > 0) {
                var cityDetails = result[0];
                var cityCode = cityDetails.city_code

                var accessQuery = {
                    "distributor_nam": distributorName
                }

                DistributorModel.findOne(accessQuery, function (err, result) {
                    if (!err) {
                        var totalAccessCode = result.access_include_city_code
                        if (result.length > 0 && totalAccessCode.indexOf(cityCode) != -1) {
                            res.status(200).json({ status: true, message: "YES" })
                        } else {
                            res.status(200).json({ status: true, message: "NO" })
                        }

                    } else {
                        res.status(500).json({ status: false, result: err, message: "Error in access check" })
                    }
                })
            } else {
                res.status(500).json({ status: false, result: err, message: "City not found" })
            }
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in access check" })
        }
    })
}