var Distributors = require('../modules/distributors.js')
var City = require('../modules/cities.js')


var APIRoutes = function (app, router) {
    this.app = app;
    this.router = router
    this.distributor = new Distributors(app, router)
    this.city = new City(app, router)
    this.init()
}

module.exports = APIRoutes

APIRoutes.prototype.init = function (req, res, next) {

    const self = this;

    self.router.post('/distributors/:action', function (req, res) {
        self.distributor.distributorActions(req, res)
    })

    self.router.post('/city/:action', function (req, res) {
        self.city.cityActions(req, res)
    })

    self.app.use("/hakunamatata", self.router)
}